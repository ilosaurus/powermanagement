# Politeknik Negeri Ujung Pandang (PNUP) 
### Prerequisites

Beberapa kebutuhan minimal aplikasi pendukung:

* Composer
* Apache Web Server
* PHP >= 5.3


## Authors
ILO
## License

Project ini hanya dapat digunakan dilingkungan PNUP saja dan tidak diperbolehkan untuk di gunakan di luar itu dengan alasan apapun.
Pengembangan bersifat terbuka bagi seluruh civitas maupun alumni PNUP.