-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2017 at 10:21 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `power`
--

-- --------------------------------------------------------

--
-- Table structure for table `ala_carte`
--

CREATE TABLE `ala_carte` (
  `id_ala_carte` int(11) NOT NULL,
  `nama_ala_carte` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ala_carte`
--

INSERT INTO `ala_carte` (`id_ala_carte`, `nama_ala_carte`) VALUES
(1, 'Waffle'),
(2, 'Pancake');

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `id_controllers` int(11) NOT NULL,
  `id_modules` varchar(60) NOT NULL,
  `identifier` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `controllers`
--

INSERT INTO `controllers` (`id_controllers`, `id_modules`, `identifier`) VALUES
(1, 'dashboard', 'dashboard'),
(3, 'makanan', 'makanan'),
(4, 'ala_carte', 'ala_carte'),
(5, 'minuman', 'minuman'),
(6, 'login', 'login'),
(7, 'sys', 'division'),
(8, 'sys', 'methods'),
(9, 'sys', 'module'),
(10, 'sys', 'permissions'),
(11, 'sys', 'privilages'),
(12, 'sys', 'roles'),
(13, 'sys', 'users');

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id_division` int(11) NOT NULL,
  `nama_division` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id_division`, `nama_division`) VALUES
(1, 'Makanan'),
(2, 'Minuman'),
(3, 'Ala Carte'),
(4, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `id_makanan` int(11) NOT NULL,
  `nama_makanan` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `makanan`
--

INSERT INTO `makanan` (`id_makanan`, `nama_makanan`) VALUES
(1, 'Nasi Goreng'),
(2, 'Bakso Kuah'),
(3, 'uu6u6');

-- --------------------------------------------------------

--
-- Table structure for table `methods`
--

CREATE TABLE `methods` (
  `id_methods` int(11) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `identifier` varchar(60) DEFAULT NULL,
  `id_controllers` int(11) NOT NULL,
  `id_modules` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `methods`
--

INSERT INTO `methods` (`id_methods`, `status`, `identifier`, `id_controllers`, `id_modules`) VALUES
(1, 1, 'index', 1, 'dashboard'),
(6, 1, 'index', 3, 'makanan'),
(7, 1, 'edit', 3, 'makanan'),
(8, 1, 'delete', 3, 'makanan'),
(9, 1, 'index', 4, 'ala_carte'),
(10, 1, 'edit', 4, 'ala_carte'),
(11, 1, 'delete', 4, 'ala_carte'),
(12, 1, 'index', 5, 'minuman'),
(13, 1, 'edit', 5, 'minuman'),
(14, 1, 'delete', 5, 'minuman'),
(15, 1, 'index', 6, 'login'),
(16, 1, 'logout', 6, 'login'),
(17, 1, 'index', 7, 'sys'),
(18, 1, 'edit', 7, 'sys'),
(19, 1, 'delete', 7, 'sys'),
(20, 1, 'index', 8, 'sys'),
(21, 1, 'delete', 8, 'sys'),
(22, 1, 'index', 9, 'sys'),
(23, 1, 'edit', 9, 'sys'),
(24, 1, 'delete', 9, 'sys'),
(25, 1, 'refresh', 9, 'sys'),
(26, 1, 'index', 10, 'sys'),
(27, 1, 'edit', 10, 'sys'),
(28, 1, 'index', 11, 'sys'),
(29, 1, 'edit', 11, 'sys'),
(30, 1, 'action_edit', 11, 'sys'),
(31, 1, 'index', 12, 'sys'),
(32, 1, 'edit', 12, 'sys'),
(33, 1, 'delete', 12, 'sys'),
(34, 1, 'index', 13, 'sys'),
(35, 1, 'edit', 13, 'sys'),
(36, 1, 'delete', 13, 'sys'),
(37, 1, 'refresh', 13, 'sys');

-- --------------------------------------------------------

--
-- Table structure for table `minuman`
--

CREATE TABLE `minuman` (
  `id_minuman` int(11) NOT NULL,
  `nama_minuman` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `minuman`
--

INSERT INTO `minuman` (`id_minuman`, `nama_minuman`) VALUES
(1, 'Air Mineral'),
(2, 'Coca Cola'),
(3, 'Es Teh Manis');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id_modules` varchar(60) NOT NULL,
  `nama_modules` varchar(60) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id_modules`, `nama_modules`, `deskripsi`) VALUES
('ala_carte', 'Ala Carte', ''),
('dashboard', 'Dashboard', 'Modul ini digunakan untuk menampilkan halaman utama kepada users'),
('login', 'Login', ''),
('makanan', 'Makanan', 'Menu daftar makanan'),
('minuman', 'Minuman', ''),
('sys', 'Sys', '');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id_permissions` bigint(20) NOT NULL,
  `id_roles` int(11) NOT NULL,
  `id_methods` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id_permissions`, `id_roles`, `id_methods`) VALUES
(1, 3, 6),
(5, 3, 12),
(6, 3, 15),
(7, 3, 16),
(8, 3, 1),
(9, 3, 9),
(10, 2, 12),
(11, 2, 13),
(12, 2, 14),
(13, 2, 8),
(14, 2, 7),
(15, 2, 6),
(16, 2, 15),
(17, 2, 16),
(18, 2, 1),
(19, 2, 11),
(20, 2, 10),
(21, 2, 9),
(22, 1, 37),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 1, 22),
(38, 1, 21),
(39, 1, 20),
(40, 1, 19),
(41, 1, 18),
(42, 1, 17),
(43, 1, 12),
(44, 1, 13),
(45, 1, 14),
(46, 1, 8),
(47, 1, 7),
(48, 1, 6),
(49, 1, 15),
(50, 1, 16),
(51, 1, 1),
(52, 1, 11),
(53, 1, 10),
(54, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `privileges`
--

CREATE TABLE `privileges` (
  `id_privileges` bigint(20) NOT NULL,
  `actions` varchar(60) DEFAULT NULL,
  `id_permissions` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privileges`
--

INSERT INTO `privileges` (`id_privileges`, `actions`, `id_permissions`) VALUES
(11, 'add', 21),
(12, 'edit', 21),
(13, 'delete', 21),
(14, 'add', 15),
(15, 'edit', 15),
(16, 'delete', 15),
(17, 'add', 10),
(18, 'edit', 10),
(19, 'delete', 10),
(20, 'add', 54),
(21, 'edit', 54),
(22, 'delete', 54),
(23, 'add', 43),
(24, 'edit', 43),
(25, 'delete', 43),
(26, 'add', 31),
(27, 'edit', 31),
(28, 'delete', 31),
(29, 'add', 28),
(30, 'edit', 28),
(31, 'delete', 28),
(32, 'add', 34),
(33, 'edit', 34),
(34, 'delete', 34),
(35, 'add', 42),
(36, 'edit', 42),
(37, 'delete', 42),
(38, 'add', 48),
(39, 'edit', 48),
(40, 'delete', 48),
(41, 'add', 39),
(42, 'edit', 39),
(43, 'delete', 39),
(44, 'add', 37),
(45, 'edit', 37),
(46, 'delete', 37),
(47, 'add', 26),
(48, 'edit', 26),
(49, 'delete', 26),
(50, 'add', 29),
(51, 'edit', 29),
(52, 'add', 32),
(53, 'edit', 32),
(54, 'add', 23),
(55, 'edit', 23),
(56, 'add', 27),
(57, 'edit', 27),
(58, 'add', 30),
(59, 'edit', 30),
(60, 'add', 35),
(61, 'edit', 35),
(62, 'add', 53),
(63, 'edit', 53),
(64, 'add', 41),
(65, 'edit', 41),
(66, 'add', 44),
(67, 'edit', 44),
(68, 'add', 47),
(69, 'edit', 47),
(70, 'add', 11),
(71, 'edit', 11),
(72, 'add', 20),
(73, 'edit', 20),
(74, 'add', 14),
(75, 'edit', 14);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id_roles` int(11) NOT NULL,
  `nama_roles` varchar(50) DEFAULT NULL,
  `identifier` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_roles`, `nama_roles`, `identifier`) VALUES
(1, 'General Manager', 'gm'),
(2, 'Supervisor', 'spv'),
(3, 'Waiters', 'wtrs');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(80) NOT NULL,
  `nama_users` varchar(60) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `id_roles` int(11) NOT NULL,
  `id_division` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `nama_users`, `password`, `status`, `id_roles`, `id_division`) VALUES
('admin', 'Aya Shalkar', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 4),
('spv_ala', 'Regina', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 2, 3),
('spv_makan', 'Kevin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 2, 1),
('spv_minum', 'Budi Anduk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 2, 2),
('wait_ala', 'Hariyono', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 3, 3),
('wait_makan', 'Kino Setiawan', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 3, 1),
('wait_minum', 'Mulyadi', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ala_carte`
--
ALTER TABLE `ala_carte`
  ADD PRIMARY KEY (`id_ala_carte`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id_controllers`),
  ADD KEY `fk_controllers_modules_idx` (`id_modules`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id_division`);

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`id_makanan`);

--
-- Indexes for table `methods`
--
ALTER TABLE `methods`
  ADD PRIMARY KEY (`id_methods`),
  ADD KEY `fk_methods_controllers1_idx` (`id_controllers`),
  ADD KEY `fk_methods_modules1_idx` (`id_modules`);

--
-- Indexes for table `minuman`
--
ALTER TABLE `minuman`
  ADD PRIMARY KEY (`id_minuman`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id_modules`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id_permissions`),
  ADD KEY `fk_permissions_roles1_idx` (`id_roles`),
  ADD KEY `fk_permissions_methods1_idx` (`id_methods`);

--
-- Indexes for table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id_privileges`),
  ADD KEY `fk_privileges_permissions1_idx` (`id_permissions`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_roles`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `fk_users_roles1_idx` (`id_roles`),
  ADD KEY `fk_users_division1_idx` (`id_division`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ala_carte`
--
ALTER TABLE `ala_carte`
  MODIFY `id_ala_carte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id_controllers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id_division` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `makanan`
--
ALTER TABLE `makanan`
  MODIFY `id_makanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `methods`
--
ALTER TABLE `methods`
  MODIFY `id_methods` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `minuman`
--
ALTER TABLE `minuman`
  MODIFY `id_minuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id_permissions` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id_privileges` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id_roles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `controllers`
--
ALTER TABLE `controllers`
  ADD CONSTRAINT `fk_controllers_modules` FOREIGN KEY (`id_modules`) REFERENCES `modules` (`id_modules`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `methods`
--
ALTER TABLE `methods`
  ADD CONSTRAINT `fk_methods_controllers1` FOREIGN KEY (`id_controllers`) REFERENCES `controllers` (`id_controllers`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_methods_modules1` FOREIGN KEY (`id_modules`) REFERENCES `modules` (`id_modules`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `fk_permissions_methods1` FOREIGN KEY (`id_methods`) REFERENCES `methods` (`id_methods`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_permissions_roles1` FOREIGN KEY (`id_roles`) REFERENCES `roles` (`id_roles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `privileges`
--
ALTER TABLE `privileges`
  ADD CONSTRAINT `fk_privileges_permissions1` FOREIGN KEY (`id_permissions`) REFERENCES `permissions` (`id_permissions`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_division1` FOREIGN KEY (`id_division`) REFERENCES `division` (`id_division`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_roles1` FOREIGN KEY (`id_roles`) REFERENCES `roles` (`id_roles`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
