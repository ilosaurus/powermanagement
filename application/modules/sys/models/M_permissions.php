<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_permissions extends CI_Model
{
    private $tabel='permissions';

    function get($u=null,$i=null,$f=null){
        if($u!=null)
            $this->db->where('id_permissions', $u);
        if($i!=null)
            $this->db->where('id_roles', $i);
        if($f!=null)
            $this->db->where('id_methods', $f);

        $this->db->order_by('id_roles','DESC');
        $query = $this->db->get($this->tabel);
        if($query)
            return $query->result();
        else
            return false;
    }

    function update($data, $id){
        $query=$this->db->update($this->tabel, $data, $id);
        if($query)
            return true;
        else
            return false;
    }

    function delete($id){
        $this->db->where('id_permissions', $id);
        $query = $this->db->delete($this->tabel);
        if($query)
            return true;
        else
            return false;
    }

    function insert($data){
        $query=$this->db->insert($this->tabel, $data);
        if($query)
            return true;
        else
            return false;
    }
}