<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_methods extends CI_Model
{
    private $tabel='methods';

    function get($u=null,$i=null,$f=null,$c=null,$s=null){
        if($u!=null)
            $this->db->where('id_methods', $u);
        if($i!=null)
            $this->db->where('id_modules', $i);
        if($f!=null)
            $this->db->where('identifier', $f);
        if($c!=null)
            $this->db->where('id_controllers', $c);
        if($s!=null)
            $this->db->where('status', $s);

        $this->db->order_by('id_modules','DESC');
        $query = $this->db->get($this->tabel);
        if($query)
            return $query->result();
        else
            return false;
    }

    function update($data, $id){
        $query=$this->db->update($this->tabel, $data, $id);
        if($query)
            return true;
        else
            return false;
    }

    function delete($id){
        $this->db->where('id_methods', $id);
        $query = $this->db->delete($this->tabel);
        if($query)
            return true;
        else
            return false;
    }

    function insert($data){
        $query=$this->db->insert($this->tabel, $data);
        if($query)
            return $this->db->insert_id();
        else
            return false;
    }
}