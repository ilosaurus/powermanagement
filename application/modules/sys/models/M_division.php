<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_division extends CI_Model
{
    private $tabel='division';

    function get($u=null){
        if($u!=null)
            $this->db->where('id_division', $u);

        $this->db->order_by('id_division','DESC');
        $query = $this->db->get($this->tabel);
        if($query)
            return $query->result();
        else
            return false;
    }

    function update($data, $id){
        $query=$this->db->update($this->tabel, $data, $id);
        if($query)
            return true;
        else
            return false;
    }

    function delete($id){
        $this->db->where('id_division', $id);
        $query = $this->db->delete($this->tabel);
        if($query)
            return true;
        else
            return false;
    }

    function insert($data){
        $query=$this->db->insert($this->tabel, $data);
        if($query)
            return true;
        else
            return false;
    }
}