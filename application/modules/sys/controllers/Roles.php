<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends HC_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_roles->get();
			foreach($q as $row){
				$action=null;
				if(in_array('edit',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/roles/edit').'/'.$row->id_roles.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit Entri" class="btn btn-xs btn-blue"><i class="entypo-pencil"></i></a>';
				if(in_array('delete',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/roles/delete').'/'.$row->id_roles.'" onclick="return confirm(\'Apakah Anda ingin menghapus data ini?\')" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Entri" class="btn btn-xs btn-danger"><i class="entypo-trash"></i></a>';
				$html.='<tr class="odd gradeX">
							<td class="text-center">'.$no.'</td>
							<td>'.$row->nama_roles.'</td>
							<td>'.$row->identifier.'</td>
							<td class="text-center"><div class="btn-group">'.$action.'</div></td>
						</tr>';
				$no++;
			}
			$data['html'] = $html;
			$data['title'] = "Daftar Roles";
			$data['content'] = "V_roles_list";
			if(in_array('add',$this->acc_privilages))
				$data['action'] = '<a href="#" data-toggle="modal" data-target="#modAdd" class="btn btn-blue btn-icon">Tambah Data<i class="entypo-plus"></i></a>';
			$this->load->view('backend', $data);
		}else{
			$i1=$this->input->post('input1',TRUE);
			$i2=$this->input->post('input2',TRUE);
			$data=array('nama_roles'=>$i1,'identifier'=>$i2);
			$ins=$this->M_roles->insert($data);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}
	
	function edit($id=null){
		if($id==null)
			show_404();

		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_roles->get($id);
			if($q){
				$data['id'] = $id;
				$data['q'] = $q;
				$data['title'] = "Edit Roles";
				$data['content'] = "V_roles_edit";
				if(in_array('add',$this->acc_privilages))
					$data['action'] = '<a href="#" data-toggle="modal" data-target="#modAdd" class="btn btn-blue btn-icon">Tambah Data<i class="entypo-plus"></i></a>';
				$this->load->view('backend', $data);
			}else
				show_404();
		}else{
			$i1=$this->input->post('id',TRUE);
			$i2=$this->input->post('input1',TRUE);
			$i3=$this->input->post('input2',TRUE);
			$idx['id_roles']=$i1;
			$data=array('nama_roles'=>$i2,'identifier'=>$i3);
			$ins=$this->M_roles->update($data,$idx);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}

	function delete($id=null){
		if($id==null)
			show_404();
		else{
			$ins=$this->M_roles->delete($id);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect(base_url($this->sys_module.'/roles'),'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect(base_url($this->sys_module.'/roles'),'refresh');
			}
		}
	}
}
