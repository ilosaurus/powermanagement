<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Methods extends HC_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_methods->get();
			foreach($q as $row){
				$action=null;
				if(in_array('delete',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/methods/delete').'/'.$row->id_methods.'" onclick="return confirm(\'Apakah Anda ingin menghapus data ini?\')" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Entri" class="btn btn-xs btn-danger"><i class="entypo-trash"></i></a>';

				$m=$this->M_modules->get($row->id_modules);
				if($m)
					$mod='<b>'.$m[0]->nama_modules.'</b><br/>Identifier: '.$m[0]->id_modules.'<br/>'.$m[0]->deskripsi;
				else
					$mod='N/A';

				$cc=$this->M_controller->get($row->id_controllers);
				if($m)
					$cid='<b>'.ucwords($cc[0]->identifier).'</b>';
				else
					$cid='N/A';

				$html.='<tr class="odd gradeX">
							<td style="vertical-align: middle" class="text-center">'.$no.'</td>
							<td style="vertical-align: middle">'.$mod.'</td>
							<td style="vertical-align: middle" class="text-center">'.$cid.'</td>
							<td style="vertical-align: middle" class="text-center">'.ucwords($row->identifier).'</td>
							<td style="vertical-align: middle" class="text-center"><div class="btn-group">'.$action.'</div></td>
						</tr>';
				$no++;
			}
			$data['html'] = $html;
			$data['title'] = "Daftar Methods";
			$data['content'] = "V_methods_list";
			$this->load->view('backend', $data);
		}else{
			$i1=$this->input->post('input1',TRUE);
			$data=array('nama_methods'=>$i1);
			$ins=$this->M_methods->insert($data);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}

	function delete($id=null){
		if($id==null)
			show_404();
		else{
			$ins=$this->M_methods->delete($id);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect(base_url($this->sys_module.'/methods'),'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect(base_url($this->sys_module.'/methods'),'refresh');
			}
		}
	}
}
