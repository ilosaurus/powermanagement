<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Privilages extends HC_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_roles->get();
			$j_mt=count($this->M_methods->get());
			foreach($q as $row){
				$action=null;
				if(in_array('edit',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/privilages/edit').'/'.$row->id_roles.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit Entri" class="btn btn-xs btn-blue"><i class="entypo-pencil"></i></a>';
				$j_pm=count($this->M_permissions->get(null,$row->id_roles));
				$j_us=count($this->M_login->get(null,null,null,$row->id_roles));
				$html.='<tr class="odd gradeX">
							<td style="vertical-align: middle" class="text-center">'.$no.'</td>
							<td style="vertical-align: middle"><b class="text-info">'.$row->nama_roles.'</b><br/>Users: '.$j_us.'</td>
							<td style="vertical-align: middle" class="text-center">'.$j_pm.'/'.$j_mt.'</td>
							<td style="vertical-align: middle" class="text-center"><div class="btn-group">'.$action.'</div></td>
						</tr>';
				$no++;
			}
			$data['html'] = $html;
			$data['title'] = "Daftar Privilages";
			$data['content'] = "V_permissions_list";
			$this->load->view('backend', $data);
		}
	}

	function edit($id=null){
		if($id==null)
			show_404();

		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_roles->get($id);
			if($q){
				$html=null;$html2=null;
				$data['id'] = $id;
				$data['q'] = $q;

				$p=$this->M_permissions->get(null,$id);
				foreach($p as $rowp){
					$mtd=$this->M_methods->get($rowp->id_methods);
					foreach($mtd as $row){
						$m=$this->M_modules->get($row->id_modules);
						if($m){
							$nm='Module '.ucwords($m[0]->nama_modules);
							$nw='<b class="text-info">'.ucwords($m[0]->nama_modules).'</b><br/>Identifier: '.$m[0]->id_modules.'<br/>Deskripsi: '.$m[0]->deskripsi;
						}else{
							$nm='Module Unknown';
							$nw=null;
						}

						$c=$this->M_controller->get($row->id_controllers);
						if($m)
							$nc=ucwords($c[0]->identifier);
						else
							$nc='Unknown';

						$cp=$this->M_permissions->get(null,$id,$row->id_methods);
						if($cp)
							$sel='selected';
						else
							$sel=null;

						$priv=$this->M_privileges->get(null,$rowp->id_permissions);$act=null;
						foreach($priv as $prrow){
							$act.=$prrow->actions.' ';
						}
						if($act==null){
							$html.='<option value="'.$rowp->id_permissions.'" '.$sel.'>'.$nm.': '.$nc.' &middot; '.ucwords($row->identifier).'</option>';
						}

						if($act!=null){
							$action=null;
							if(in_array('edit',$this->acc_privilages))
								$action.='<a href="'.base_url($this->sys_module.'/privilages/action_edit').'/'.$rowp->id_permissions.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit Entri" class="btn btn-xs btn-blue"><i class="entypo-pencil"></i></a>';

							$html2.='<tr class="odd gradeX">
										<td style="vertical-align: middle" class="text-center">'.$no.'</td>
										<td style="vertical-align: middle">'.$nw.'</td>
										<td style="vertical-align: middle" class="text-center">'.$nc.' &middot; '.$mtd[0]->identifier.'</td>
										<td style="vertical-align: middle" class="text-center">'.$act.'</td>
										<td style="vertical-align: middle" class="text-center"><div class="btn-group">'.$action.'</div></td>
									</tr>';
							$no++;
						}
					}
				}

				$data['permis']=$html;
				$data['html']=$html2;
				$data['title'] = "Edit Privilages";
				if(in_array('add',$this->acc_privilages))
					$data['action'] = '<a href="'.base_url('sys/permissions/edit/'.$id).'" class="btn btn-blue btn-icon">Tambah Permission<i class="entypo-plus"></i></a>';
				$data['content'] = "V_privilages_edit";
				$this->load->view('backend', $data);
			}else
				show_404();
		}else{
			$per=$this->input->post('permissions',TRUE);
			$pri=$this->input->post('actions',TRUE);
			$fail=false;

			if(!empty($pri)) {
				foreach ($pri as $row) {
					if (!$this->M_privileges->get(null, $per, $row)) {
						$data = array('id_permissions' => $per, 'actions' => $row);
						$i = $this->M_privileges->insert($data);
						if (!$i) $fail = true;
					}
				}
			}

			if(!$fail){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}

	function action_edit($id=null){
		if($id==null)
			show_404();

		if(!$this->input->post('submit')){
			$html=null;$html2=null;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_permissions->get($id);
			if($q){
				$data['id'] = $id;
				$data['q'] = $q;

				$mtd=$this->M_methods->get($q[0]->id_methods);
				if($mtd){
					$m=$this->M_modules->get($mtd[0]->id_modules);
					if($m)
						$nm='Module '.ucwords($m[0]->nama_modules);
					else
						$nm='Module Unknown';

					$c=$this->M_controller->get($mtd[0]->id_controllers);
					if($m)
						$nc=ucwords($c[0]->identifier);
					else
						$nc='Unknown';

					$priv=$this->M_privileges->get(null,$q[0]->id_permissions);$x=array();
					foreach($priv as $prrow)
						array_push($x,$prrow->actions);

					$html.=$nm.' -- '.$nc.' -- '.ucwords($mtd[0]->identifier);
				}

				if(in_array('add',$x))
					$html2.='<option value="add" selected>ADD</option>';
				else
					$html2.='<option value="add">ADD</option>';

				if(in_array('edit',$x))
					$html2.='<option value="edit" selected>EDIT</option>';
				else
					$html2.='<option value="edit">EDIT</option>';

				if(in_array('delete',$x))
					$html2.='<option value="delete" selected>DELETE</option>';
				else
					$html2.='<option value="delete">DELETE</option>';

				$data['roles']=$this->M_roles->get($q[0]->id_roles);
				$data['html']=$html;
				$data['html2']=$html2;
				$data['title'] = "Edit Privilages";
				if(in_array('add',$this->acc_privilages))
					$data['action'] = '<a href="'.base_url('sys/permissions/edit/'.$id).'" class="btn btn-blue btn-icon">Tambah Permission<i class="entypo-plus"></i></a>';
				$data['content'] = "V_privilages_edit_entry";
				$this->load->view('backend', $data);
			}else
				show_404();
		}else{
			$id=$this->input->post('id',TRUE);
			$per=$this->input->post('actions',TRUE);
			$fail=false;

			$a_un=array();
			if(!empty($per)){
				foreach($per as $row){
					array_push($a_un,$row);
					if(!$this->M_privileges->get(null,$id,$row)){
						$data=array('id_permissions'=>$id,'actions'=>$row);
						$i=$this->M_privileges->insert($data);
						if(!$i) $fail=true;
					}
				}
			}
			if(!in_array('add',$a_un)){
				if($qc=$this->M_privileges->get(null,$id,'add')){
					$dd=$this->M_privileges->delete($qc[0]->id_privileges);
					if(!$dd) $fail=true;
				}
			}
			if(!in_array('edit',$a_un)){
				if($qc=$this->M_privileges->get(null,$id,'edit')){
					$dd=$this->M_privileges->delete($qc[0]->id_privileges);
					if(!$dd) $fail=true;
				}
			}
			if(!in_array('delete',$a_un)){
				if($qc=$this->M_privileges->get(null,$id,'delete')){
					$dd=$this->M_privileges->delete($qc[0]->id_privileges);
					if(!$dd) $fail=true;
				}
			}

			if(!$fail){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}
}
