<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends HC_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_roles->get();
			$j_mt=count($this->M_methods->get());
			foreach($q as $row){
				$action=null;
				if(in_array('edit',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/permissions/edit').'/'.$row->id_roles.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit Entri" class="btn btn-xs btn-blue"><i class="entypo-pencil"></i></a>';
				$j_pm=count($this->M_permissions->get(null,$row->id_roles));
				$j_us=count($this->M_login->get(null,null,null,$row->id_roles));
				$html.='<tr class="odd gradeX">
							<td style="vertical-align: middle" class="text-center">'.$no.'</td>
							<td style="vertical-align: middle"><b class="text-info">'.$row->nama_roles.'</b><br/>Users: '.$j_us.'</td>
							<td style="vertical-align: middle" class="text-center">'.$j_pm.'/'.$j_mt.'</td>
							<td style="vertical-align: middle" class="text-center"><div class="btn-group">'.$action.'</div></td>
						</tr>';
				$no++;
			}
			$data['html'] = $html;
			$data['title'] = "Daftar Permissions";
			$data['content'] = "V_permissions_list";
			$this->load->view('backend', $data);
		}
	}

	function edit($id=null){
		if($id==null)
			show_404();

		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_roles->get($id);
			if($q){
				$html=null;
				$data['id'] = $id;
				$data['q'] = $q;

				$p=$this->M_methods->get();
				foreach($p as $row){
					$m=$this->M_modules->get($row->id_modules);
					if($m)
						$nm='Module '.ucwords($m[0]->nama_modules);
					else
						$nm='Module Unknown';

					$c=$this->M_controller->get($row->id_controllers);
					if($m)
						$nc=ucwords($c[0]->identifier);
					else
						$nc='Unknown';

					$cp=$this->M_permissions->get(null,$id,$row->id_methods);
					if($cp)
						$sel='selected';
					else
						$sel=null;
					$html.='<option value="'.$row->id_methods.'" '.$sel.'>'.$nm.': '.$nc.' &middot; '.ucwords($row->identifier).'</option>';
				}
				$data['permis']=$html;
				$data['title'] = "Edit Permissions";
				$data['content'] = "V_permissions_edit";
				$this->load->view('backend', $data);
			}else
				show_404();
		}else{
			$id=$this->input->post('id',TRUE);
			$per=$this->input->post('permission',TRUE);
			$fail=false;

			$a_un=array();
			if(!empty($per)) {
				foreach ($per as $row) {
					array_push($a_un, $row);
					//TODO: Masukkan entri baru ke database
					if (!$this->M_permissions->get(null, $id, $row)) {
						$data = array('id_roles' => $id, 'id_methods' => $row);
						$i = $this->M_permissions->insert($data);
						if (!$i) $fail = true;
					}
				}
			}
			$p=$this->M_methods->get();
			foreach($p as $rp){
				if(!in_array($rp->id_methods,$a_un)){
					//Tidak ada didaftar, apakah sudah ada d database? kalo belum ada skip, kalo sudah ada hapus entri
					if($qc=$this->M_permissions->get(null,$id,$rp->id_methods)){
						$dd=$this->M_permissions->delete($qc[0]->id_permissions);
						if(!$dd) $fail=true;
					}
				}
			}
			if(!$fail){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}
}
