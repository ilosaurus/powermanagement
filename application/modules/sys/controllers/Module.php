<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends HC_Controller {

	private $m_path=null;

	function __construct(){
		parent::__construct();
		$ds=DIRECTORY_SEPARATOR;
		$this->m_path=getcwd().$ds."application".$ds."modules".$ds;
	}

	function index(){
		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_modules->get();
			foreach($q as $row){
				$action=null;
				$action.='<a href="'.base_url($this->sys_module.'/module/refresh').'/'.$row->id_modules.'" onclick="return confirm(\'Apakah Anda ingin memperbarui module ini?\')" data-toggle="tooltip" data-placement="top" title="Refresh Entri" class="btn btn-xs btn-primary"><i class="entypo-arrows-ccw"></i></a>';
				if(in_array('edit',$this->acc_privilages))
				$action.='<a href="'.base_url($this->sys_module.'/module/edit').'/'.$row->id_modules.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit Entri" class="btn btn-xs btn-blue"><i class="entypo-pencil"></i></a>';
				if(in_array('delete',$this->acc_privilages))
				$action.='<a href="'.base_url($this->sys_module.'/module/delete').'/'.$row->id_modules.'" onclick="return confirm(\'Apakah Anda ingin menghapus data ini?\')" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Entri" class="btn btn-xs btn-danger"><i class="entypo-trash"></i></a>';
				$html.='<tr class="odd gradeX">
							<td style="vertical-align:middle" class="text-center">'.$no.'</td>
							<td style="vertical-align:middle"><b>'.$row->nama_modules.'</b><br/>Identifier: '.$row->id_modules.'</td>
							<td style="vertical-align:middle">'.$row->deskripsi.'</td>
							<td style="vertical-align:middle" class="text-center"><div class="btn-group">'.$action.'</div></td>
						</tr>';
				$no++;
			}

			$directories = glob($this->m_path.'*' , GLOB_ONLYDIR);$opt=null;
			foreach($directories as $row) {
				$c_path = str_replace($this->m_path, "", $row);
				$q = $this->M_modules->get($c_path);
				if(!$q){
					$opt.='<option value="'.$c_path.'">'.ucwords($c_path).'</option>';
				}
			}
			$data['opt'] = $opt;
			$data['html'] = $html;
			$data['title'] = "Daftar Modules";
			$data['content'] = "V_modules_list";
			if(in_array('add',$this->acc_privilages))
				$data['action'] = '<a href="#" data-toggle="modal" data-target="#modAdd" class="btn btn-blue btn-icon">Tambah Data<i class="entypo-plus"></i></a>';
			$this->load->view('backend', $data);
		}else{
			$i1=$this->input->post('input1',TRUE);
			$i2=$this->input->post('input2',TRUE);
			$rx=str_replace('_',' ',$i1); $fail=false;

			//TODO: Simpan modul baru kedalam database, kemudian:
			$data=array('id_modules'=>$i1,'nama_modules'=>ucwords($rx),'deskripsi'=>$i2);
			$ins=$this->M_modules->insert($data);
			if($ins){
				//TODO: Get semua controller yang ada dalam modul yang dipilih dari form
				$ac_path=$this->m_path.DIRECTORY_SEPARATOR.$i1.DIRECTORY_SEPARATOR."controllers";
				$gc = array_diff(scandir($ac_path), array('.', '..', 'index.html'));
				foreach ($gc as $racess) {
					$rc=strtolower(str_replace(".php","",$racess));
					$data=array('identifier'=>$rc,'id_modules'=>$i1);
					$ins=$this->M_controller->insert($data);
					if($ins){
						//TODO: Get semua methods yang ada dalam semua controllers dari modul dipilih dari form
						$k=$this->_get_functions($this->m_path.DIRECTORY_SEPARATOR.$i1.DIRECTORY_SEPARATOR."controllers".DIRECTORY_SEPARATOR.$racess);
						if($k){
							$data=array();
							foreach($k as $rka){
								//TODO: Remove construct dan private function
								if($rka!='__construct' and substr($rka,0,1)!='_' and $rka!='if'){
									$data=array('identifier'=>$rka,'id_controllers'=>$ins,'id_modules'=>$i1,'status'=>1);
									$insa=$this->M_methods->insert($data);
									if(!$insa) $fail=true;
								}
							}
						}
					}else
						$fail=true;
				}
			}else
				$fail=true;

			if(!$fail){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}
	
	function edit($id=null){
		if($id==null)
			show_404();

		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_modules->get($id);
			if($q){
				$data['id'] = $id;
				$data['q'] = $q;
				$data['title'] = "Edit Modules";
				$data['content'] = "V_modules_edit";
				if(in_array('add',$this->acc_privilages))
					$data['action'] = '<a href="#" data-toggle="modal" data-target="#modAdd" class="btn btn-blue btn-icon">Tambah Data<i class="entypo-plus"></i></a>';
				$this->load->view('backend', $data);
			}else
				show_404();
		}else{
			$i1=$this->input->post('id',TRUE);
			$i2=$this->input->post('input1',TRUE);
			$i3=$this->input->post('input2',TRUE);
			$idx['id_modules']=$i1;
			$data=array('nama_modules'=>$i2,'deskripsi'=>$i3);
			$ins=$this->M_modules->update($data,$idx);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}

	function delete($id=null){
		if($id==null)
			show_404();
		else{
			$ins=$this->M_modules->delete($id);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect(base_url($this->sys_module.'/module'),'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect(base_url($this->sys_module.'/module'),'refresh');
			}
		}
	}

	function refresh($id=null){
		if($id==null)
			show_404();
		else{
			$q=$this->M_modules->get($id);
			if($q){
				$fail=false;
				//TODO: Get semua controller yang ada dalam modul yang dipilih dari form
				$ac_path=$this->m_path.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR."controllers";
				$gc = array_diff(scandir($ac_path), array('.', '..', 'index.html'));
				foreach ($gc as $racess) {
					$rc = strtolower(str_replace(".php", "", $racess));
					if (!$gct = $this->M_controller->get(null, $rc, $id)) {
						$data = array('identifier' => $rc, 'id_modules' => $id);
						$ins = $this->M_controller->insert($data);
						if (!$ins) $fail = true;
					}

					//TODO: Get semua methods yang ada dalam semua controllers dari modul dipilih dari form
					$k = $this->_get_functions($this->m_path . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . "controllers" . DIRECTORY_SEPARATOR . $racess);
					if ($k) {
						$data = array();
						foreach ($k as $rka) {
							//TODO: Remove construct dan private function
							if ($rka != '__construct' and substr($rka, 0, 1) != '_' and $rka != 'if') {
								$ccc = $this->M_controller->get(null, $rc, $id);
								if ($ccc)
									$id_con = $ccc[0]->id_controllers;
								else
									$id_con = null;

								if (!$gmtd = $this->M_methods->get(null, $id, $rka, $id_con)) {
									$data = array('identifier' => $rka, 'id_controllers' => $rc, 'id_modules' => $id, 'status' => 1);
									$ins = $this->M_methods->insert($data);
									if (!$ins) $fail = true;
								}
							}
						}
					}
				}
				if(!$fail){
					$this->session->set_flashdata('stat', 's');
					redirect(base_url($this->sys_module.'/module'),'refresh');
				}else{
					$this->session->set_flashdata('stat', 'f');
					redirect(base_url($this->sys_module.'/module'),'refresh');
				}
			}
		}
	}
}
