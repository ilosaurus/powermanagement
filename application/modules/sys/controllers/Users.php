<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends HC_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');
			elseif($in!=null and $in!='s' and $in!='f')
				$data['alert'] = array('title'=>'Password Berhasil Diganti','state'=>'success','msg'=>'Password Baru: password');

			$q=$this->M_login->get();
			foreach($q as $row){
				$action=null;
				$action.='<a href="'.base_url($this->sys_module.'/users/refresh').'/'.$row->username.'" onclick="return confirm(\'Apakah Anda ingin mereset password user ini?\')" data-toggle="tooltip" data-placement="top" title="Refresh Entri" class="btn btn-xs btn-primary"><i class="entypo-arrows-ccw"></i></a>';
				if(in_array('edit',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/users/edit').'/'.$row->username.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit Entri" class="btn btn-xs btn-blue"><i class="entypo-pencil"></i></a>';
				if(in_array('delete',$this->acc_privilages))
					$action.='<a href="'.base_url($this->sys_module.'/users/delete').'/'.$row->username.'" onclick="return confirm(\'Apakah Anda ingin menghapus data ini?\')" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Entri" class="btn btn-xs btn-danger"><i class="entypo-trash"></i></a>';

				$m=$this->M_roles->get($row->id_roles);
				if($m)
					$mod=ucwords($m[0]->nama_roles);
				else
					$mod='N/A';

				$d=$this->M_division->get($row->id_division);
				if($m)
					$mods=ucwords($d[0]->nama_division);
				else
					$mods='N/A';

				if($row->status==1)
					$modsa='<span class="label label-success">AKTIF</span>';
				else
					$modsa='<span class="label label-danger">TIDAK AKTIF</span>';

				$html.='<tr class="odd gradeX">
							<td style="vertical-align: middle" class="text-center">'.$no.'</td>
							<td style="vertical-align: middle"><b class="text-info">'.$row->nama_users.'</b><br/>Username: '.$row->username.'</td>
							<td style="vertical-align: middle" class="text-center"><b class="text-info">'.$mod.'</b></td>
							<td style="vertical-align: middle" class="text-center">'.$mods.'</td>
							<td style="vertical-align: middle" class="text-center">'.$modsa.'</td>
							<td style="vertical-align: middle" class="text-center"><div class="btn-group">'.$action.'</div></td>
						</tr>';
				$no++;
			}

			$o2=$this->M_roles->get();$opt1=null;
			foreach($o2 as $row){
				$opt1.='<option value="'.$row->id_roles.'">'.ucwords($row->nama_roles).'</option>';
			}
			$o3=$this->M_division->get();$opt2=null;
			foreach($o3 as $row){
				$opt2.='<option value="'.$row->id_division.'">'.ucwords($row->nama_division).'</option>';
			}
			$data['opt1'] = $opt1;
			$data['opt2'] = $opt2;
			$data['html'] = $html;
			$data['title'] = "Daftar Users";
			$data['content'] = "V_users_list";
			if(in_array('add',$this->acc_privilages))
				$data['action'] = '<a href="#" data-toggle="modal" data-target="#modAdd" class="btn btn-blue btn-icon">Tambah Data<i class="entypo-plus"></i></a>';
			$this->load->view('backend', $data);
		}else{
			$i1=$this->input->post('input1',TRUE);
			$i2=$this->input->post('input2',TRUE);
			$i3=$this->input->post('input3');
			$i4=$this->input->post('input4',TRUE);
			$i5=$this->input->post('input5',TRUE);
			$i6=$this->input->post('input6',TRUE);

			$data=array('nama_users'=>$i1,'username'=>$i2,'password'=>sha1($i3),'id_roles'=>$i4,'id_division'=>$i5,'status'=>$i6);
			$ins=$this->M_login->insert($data);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}
	
	function edit($id=null){
		if($id==null)
			show_404();

		if(!$this->input->post('submit')){
			$html=null;$no=1;
			$in=$this->session->flashdata('stat');
			if($in=='s')
				$data['alert'] = array('title'=>'Data Tersimpan','state'=>'success','msg'=>'Data yang Anda masukkan berhasil tersimpan');
			elseif($in!=null and $in!='s')
				$data['alert'] = array('title'=>'Data Tidak Tersimpan','state'=>'error','msg'=>'Data yang Anda masukkan gagal disimpan');

			$q=$this->M_login->get($id);
			if($q){
				$o2=$this->M_roles->get();$opt1=null;
				foreach($o2 as $row){
					if($row->id_roles==$q[0]->id_roles)
						$ss='selected';
					else
						$ss=null;
					$opt1.='<option value="'.$row->id_roles.'" '.$ss.'>'.ucwords($row->nama_roles).'</option>';
				}
				$o3=$this->M_division->get();$opt2=null;
				foreach($o3 as $row){
					if($row->id_division==$q[0]->id_division)
						$ss='selected';
					else
						$ss=null;
					$opt2.='<option value="'.$row->id_division.'" '.$ss.'>'.ucwords($row->nama_division).'</option>';
				}

				$data['opt1'] = $opt1;
				$data['opt2'] = $opt2;
				$data['id'] = $id;
				$data['q'] = $q;
				$data['title'] = "Edit Users";
				$data['content'] = "V_users_edit";
				if(in_array('add',$this->acc_privilages))
					$data['action'] = '<a href="#" data-toggle="modal" data-target="#modAdd" class="btn btn-blue btn-icon">Tambah Data<i class="entypo-plus"></i></a>';
				$this->load->view('backend', $data);
			}else
				show_404();
		}else{
			$idd=$this->input->post('id',TRUE);
			$i1=$this->input->post('input1',TRUE);
			$i2=$this->input->post('input2',TRUE);
			$i4=$this->input->post('input4',TRUE);
			$i5=$this->input->post('input5',TRUE);
			$i6=$this->input->post('input6',TRUE);

			$data=array('nama_users'=>$i1,'username'=>$i2,'id_roles'=>$i4,'id_division'=>$i5,'status'=>$i6);
			$idx['username']=$idd;
			$ins=$this->M_login->update($data,$idx);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect($this->current_url,'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect($this->current_url,'refresh');
			}
		}
	}

	function delete($id=null){
		if($id==null)
			show_404();
		else{
			$ins=$this->M_login->delete($id);
			if($ins){
				$this->session->set_flashdata('stat', 's');
				redirect(base_url($this->sys_module.'/users'),'refresh');
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect(base_url($this->sys_module.'/users'),'refresh');
			}
		}
	}

	function refresh($id=null){
		if($id==null)
			show_404();
		else{
			$q=$this->M_login->get($id);
			if($q){
				$idx['username']=$id;
				$data=array('password'=>sh1('password'));
				$ins=$this->M_login->update($data,$idx);
				if($ins){
					$this->session->set_flashdata('stat', 'k');
					redirect(base_url($this->sys_module.'/users'),'refresh');
				}else{
					$this->session->set_flashdata('stat', 'f');
					redirect(base_url($this->sys_module.'/users'),'refresh');
				}
			}else{
				$this->session->set_flashdata('stat', 'f');
				redirect(base_url($this->sys_module.'/users'),'refresh');
			}
		}
	}
}
