<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered datatable" id="table-1">
            <thead>
            <tr>
                <th class="text-center" style="width: 40px;">No</th>
                <th class="text-center">Nama Users</th>
                <th class="text-center">Roles</th>
                <th class="text-center">Divisi</th>
                <th class="text-center">Status</th>
                <th class="text-center" style="width: 120px;">Actions</th>
            </tr>
            </thead>
            <tbody>
                <?php echo $html; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modAdd">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Entry <?php echo $title ?></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo $this->current_url; ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama User</label>
                                <input type="text" class="form-control" name="input1" placeholder="Input text here...">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Username</label>
                                <input type="text" class="form-control" name="input2" placeholder="Input text here...">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <input type="password" class="form-control" name="input3" placeholder="Input text here...">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Role</label>
                                <select name="input4" class="form-control">
                                    <?php echo $opt1; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Divisi</label>
                                <select name="input5" class="form-control">
                                    <?php echo $opt2; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Status</label>
                                <select name="input6" class="form-control">
                                    <option value="1" selected>AKTIF</option>
                                    <option value="0">TIDAK AKTIF</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-info" name="submit" value="Simpan"/>
            </div>
            </form>
        </div>
    </div>
</div>