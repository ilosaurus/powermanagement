<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo $this->current_url; ?>">
                    <input type="hidden" name="id" value="<?php echo $id ?>"/>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Nama Users</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="input1" placeholder="Input text here..." value="<?php echo $q[0]->nama_users ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="input2" placeholder="Input text here..." value="<?php echo $q[0]->username ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Roles</label>
                        <div class="col-sm-6">
                            <select name="input4" class="form-control">
                                <?php echo $opt1; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Division</label>
                        <div class="col-sm-6">
                            <select name="input5" class="form-control">
                                <?php echo $opt2; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-6">
                            <select name="input6" class="form-control">
                                <?php if($q[0]->status==1){ ?>
                                    <option value="1" selected>AKTIF</option>
                                    <option value="0">TIDAK AKTIF</option>
                                <?php }else{ ?>
                                    <option value="1">AKTIF</option>
                                    <option value="0" selected>TIDAK AKTIF</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6 text-right">
                            <a href="<?php echo $this->reff ?>" class="btn btn-default">Kembali</a>
                            <input type="submit" class="btn btn-info" name="submit" value="Simpan"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>