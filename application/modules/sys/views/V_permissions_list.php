<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered datatable" id="table-1">
            <thead>
            <tr>
                <th class="text-center" style="width: 40px;">No</th>
                <th class="text-center">Nama Roles</th>
                <th class="text-center">Jumlah Permissions</th>
                <th class="text-center" style="width: 120px;">Actions</th>
            </tr>
            </thead>
            <tbody>
                <?php echo $html; ?>
            </tbody>
        </table>
    </div>
</div>