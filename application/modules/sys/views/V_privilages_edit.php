<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo $this->current_url; ?>">
                    <input type="hidden" name="id" value="<?php echo $id ?>"/>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Nama Roles</label>
                        <div class="col-sm-6">
                            <label for="field-1" class="text-info control-label"><?php echo ucwords($q[0]->nama_roles) ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Permissions</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="permissions" data-live-search="true">
                                <?php echo $permis; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Action Privilages</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="actions[]" multiple data-live-search="true">
                                <option value="add">ADD</option>
                                <option value="edit">EDIT</option>
                                <option value="delete">DELETE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <p class="text-justify">
                                <b>Keterangan:</b> Silakan tandai semua actions yang ingin Anda daftarkan untuk user ini.                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6 text-right">
                            <a href="<?php echo $this->reff ?>" class="btn btn-default">Kembali</a>
                            <input type="submit" class="btn btn-info" name="submit" value="Simpan"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered datatable" id="table-1">
            <thead>
            <tr>
                <th class="text-center" style="width: 40px;">No</th>
                <th class="text-center">Modules</th>
                <th class="text-center">Permissions</th>
                <th class="text-center">Privilages Actions</th>
                <th class="text-center" style="width: 120px;">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php echo $html ?>
            </tbody>
        </table>
    </div>
</div>