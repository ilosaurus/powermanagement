<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo $this->current_url; ?>">
                    <input type="hidden" name="id" value="<?php echo $id ?>"/>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Nama Roles</label>
                        <div class="col-sm-6">
                            <label for="field-1" class="text-info control-label"><?php echo ucwords($q[0]->nama_roles) ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Permissions</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="permission[]" multiple data-live-search="true">
                                <?php echo $permis; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <p class="text-justify">
                                <b>Keterangan:</b> Silakan tandai semua permission yang ingin Anda daftarkan untuk user ini dan sebaliknya
                                hapus permission dari daftar di atas apabila Anda ingin mengahpus permission tertentu dari user ini.
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6 text-right">
                            <a href="<?php echo $this->reff ?>" class="btn btn-default">Kembali</a>
                            <input type="submit" class="btn btn-info" name="submit" value="Simpan"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>