<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard_menu extends CI_Model
{
    function get_menu($level=null, $parent_id=null, $role=null)
    {
        if($level == 0)
        {
            $this->db->where('level_menu', 0);
        }
        else
        {
            $this->db->where('level_menu', $level);
            $this->db->where('id_parent', $parent_id);
        }

        $this->db->where('id_role', $role);
        $this->db->order_by('urutan', 'ASC');
        $query = $this->db->get('sidebar_menu');

        if($query)
            return $query->result();
        else
            return false;
    }
}