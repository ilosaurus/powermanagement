<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends HC_Controller {

	function __construct(){
		$this->acc_permission_bypass=1;
		parent::__construct();
	}

	public function index(){
		$data=array('title'=>'Dashboard','content'=>'V_dashboard');
		$data['j_us']=count($this->M_login->get());
		$data['j_mk']=count($this->M_makanan->get());
		$data['j_mn']=count($this->M_minuman->get());
		$data['j_al']=count($this->M_ala_carte->get());
		$this->load->view('backend',$data);
	}
}
