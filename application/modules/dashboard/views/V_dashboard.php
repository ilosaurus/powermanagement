<div class="row">
    <div class="col-sm-3 col-xs-6">
        <div class="tile-stats tile-red">
            <div class="icon"><i class="entypo-users"></i></div>
            <div class="num" data-start="0" data-end="<?php echo $j_us ?>" data-postfix="" data-duration="1500" data-delay="0">0</div>
            <h3>Registered Users</h3>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
        <div class="tile-stats tile-green">
            <div class="icon"><i class="entypo-chart-bar"></i></div>
            <div class="num" data-start="0" data-end="<?php echo $j_mk ?>" data-postfix="" data-duration="1500" data-delay="600">0</div>
            <h3>Jumlah Makanan</h3>
        </div>
    </div>
    <div class="clear visible-xs"></div>
    <div class="col-sm-3 col-xs-6">
        <div class="tile-stats tile-aqua">
            <div class="icon"><i class="entypo-chart-bar"></i></div>
            <div class="num" data-start="0" data-end="<?php echo $j_mn ?>" data-postfix="" data-duration="1500" data-delay="1200">0</div>
            <h3>Jumlah Minuman</h3>
        </div>
    </div>

    <div class="col-sm-3 col-xs-6">
        <div class="tile-stats tile-blue">
            <div class="icon"><i class="entypo-chart-bar"></i></div>
            <div class="num" data-start="0" data-end="<?php echo $j_al ?>" data-postfix="" data-duration="1500" data-delay="1800">0</div>
            <h3>Jumlah Ala Carte</h3>
        </div>
    </div>
</div>