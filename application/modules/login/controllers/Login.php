<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends HC_Controller {

	function __construct(){
		$this->acc_permission_bypass=1;
		parent::__construct();
	}

	function index(){
		if(!$this->user_logged_in){
			if(!$this->input->post('submit')){
				$data['msg'] = "";
				$this->load->view('V_login', $data);
			}else{
				$u=$this->input->post('uname', TRUE);
				$p=$this->input->post('password');

				if($q=$this->M_login->get($u,sha1($p))){
					$sess_array = array(
						'username' => $q[0]->username,
						'id_roles' => $q[0]->id_roles,
						'id_division' => $q[0]->id_division,
						'status' => $q[0]->status
					);
					$this->session->set_userdata('logged_in', $sess_array);
					redirect(base_url(),'refresh');
				}else{
					$data['msg'] = "Username dan Password Anda tidak sesuai";
					$this->load->view('V_login', $data);
				}
			}
		}else{
			echo modules::run('dashboard/dashboard/index');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}
}
