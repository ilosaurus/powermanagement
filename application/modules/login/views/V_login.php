<?php $this->load->view('includes/header') ?>
<body class="page-body login-page">
<div class="login-container">
    <div class="login-form" style="padding-top: 100px;">
        <div class="login-content">
            <a href="index.html" class="logo">
                <img src="<?php echo base_url('assets/backend') ?>/images/logo@2x.png" width="120" alt="" />
            </a>
            <br/><br/>
            <p class="description">Silakan masukkan username dan password Anda.</p>
        </div>

        <div class="login-content">
            <?php if($msg!=null){ ?>
            <div class="form-login-error" style="display: block;">
                <p><?php echo $msg; ?></p>
            </div>
            <?php } ?>

            <form method="post" role="form" id="form_login">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>
                        <input type="text" class="form-control" name="uname" id="username" placeholder="Username" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block btn-login" name="submit" value="Log In" />
                </div>
            </form>
            <p class="description" style="color: white;"><strong>&copy; 2016 - <?php echo $this->config->item('name') ?></strong> &middot; <a href="mailto:<?php echo $this->config->item('author_contact') ?>" target="_blank"><?php echo $this->config->item('author') ?></a></p>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer') ?>
