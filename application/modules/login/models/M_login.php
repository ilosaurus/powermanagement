<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model
{
    private $tabel='users';

    function get($u=null,$p=null,$id_u=null,$id_r=null,$id_d=null){
        if($u!=null)
            $this->db->where('username', $u);
        if($p!=null)
            $this->db->where('password', $p);
        if($id_u!=null)
            $this->db->where('id_user', $id_u);
        if($id_r!=null)
            $this->db->where('id_roles', $id_r);
        if($id_d!=null)
            $this->db->where('id_division', $id_d);

        $query = $this->db->get($this->tabel);
        if($query)
            return $query->result();
        else
            return false;
    }

    function update($data, $id){
        $query=$this->db->update($this->tabel, $data, $id);
        if($query)
            return true;
        else
            return false;
    }

    function delete($id){
        $this->db->where('username', $id);
        $query = $this->db->delete($this->tabel);
        if($query)
            return true;
        else
            return false;
    }

    function insert($data){
        $query=$this->db->insert($this->tabel, $data);
        if($query)
            return true;
        else
            return false;
    }
}