<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered datatable" id="table-1">
            <thead>
            <tr>
                <th class="text-center" style="width: 40px;">No</th>
                <th class="text-center">Nama Minuman</th>
                <th class="text-center" style="width: 120px;">Actions</th>
            </tr>
            </thead>
            <tbody>
                <?php echo $html; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modAdd">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Entry <?php echo $title ?></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo $this->current_url; ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama Minuman</label>
                                <input type="text" class="form-control" name="input1" placeholder="Input text here...">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-info" name="submit" value="Simpan"/>
            </div>
            </form>
        </div>
    </div>
</div>