<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_minuman extends CI_Model
{
    private $tabel='minuman';

    function get($u=null){
        if($u!=null)
            $this->db->where('id_minuman', $u);

        $this->db->order_by('id_minuman','DESC');
        $query = $this->db->get($this->tabel);
        if($query)
            return $query->result();
        else
            return false;
    }

    function update($data, $id){
        $query=$this->db->update($this->tabel, $data, $id);
        if($query)
            return true;
        else
            return false;
    }

    function delete($id){
        $this->db->where('id_minuman', $id);
        $query = $this->db->delete($this->tabel);
        if($query)
            return true;
        else
            return false;
    }

    function insert($data){
        $query=$this->db->insert($this->tabel, $data);
        if($query)
            return true;
        else
            return false;
    }
}