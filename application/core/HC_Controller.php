<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH."third_party/MX/Controller.php";

class HC_Controller extends MX_Controller {

    public $user_logged_in=false;
    public $user_name=null;
    public $user_full_name=null;
    public $user_role_id=null;
    public $user_role_name=null;
    public $user_role_identifier=null;
    public $user_division_id=null;
    public $user_status=null;
    public $acc_permission_bypass=0;
    public $acc_login_bypass=0;
    public $acc_permission=false;
    public $acc_privilages=array();
    public $sys_module=null;
    public $sys_controller=null;
    public $sys_methods=null;
    public $sys_breadcrumb=null;
    public $reff=null;
    public $current_url=null;

    function __construct(){
        parent::__construct();
        $this->load->model('login/M_login','',TRUE);
        $this->load->model('makanan/M_makanan','',TRUE);
        $this->load->model('minuman/M_minuman','',TRUE);
        $this->load->model('ala_carte/M_ala_carte','',TRUE);
        $this->load->model('sys/M_division','',TRUE);
        $this->load->model('sys/M_roles','',TRUE);
        $this->load->model('sys/M_modules','',TRUE);
        $this->load->model('sys/M_controller','',TRUE);
        $this->load->model('sys/M_methods','',TRUE);
        $this->load->model('sys/M_permissions','',TRUE);
        $this->load->model('sys/M_privileges','',TRUE);

        date_default_timezone_set('Asia/Makassar');
        $this->sys_module=$this->router->fetch_module();
        $this->sys_controller=$this->router->fetch_class();
        $this->sys_method=$this->router->fetch_method();
        $this->user_logged_in=$this->session->userdata('logged_in');

        if($this->acc_login_bypass!=1){
            if(!$this->user_logged_in && $this->sys_module!='login')
                redirect(base_url('login'),'refresh');
            else{
                $this->reff=$this->agent->referrer();
                $this->_get_user_identity();
                $this->_render_breadcrumb();
                $this->current_url=base_url(uri_string());
                if($this->acc_permission_bypass!=1){
                    $this->_check_permission();
                }
            }
        }
    }

    function _get_user_identity(){
        $this->user_name = $this->user_logged_in['username'];
        $this->user_full_name=$this->M_login->get($this->user_name)[0]->nama_users;
        $this->user_division_id = $this->user_logged_in['id_division'];
        $this->user_status = $this->user_logged_in['status'];
        $this->user_role_id = $this->user_logged_in['id_roles'];
        $this->user_role_name=$this->M_roles->get($this->user_role_id)[0]->nama_roles;
        $this->user_role_identifier=$this->M_roles->get($this->user_role_id)[0]->identifier;
    }

    function _check_permission(){
        $c=$this->M_controller->get(null,$this->sys_controller,$this->sys_module);
        if($c){
            $m=$this->M_methods->get(null,$this->sys_module,$this->sys_method,$c[0]->id_controllers);
            if($m){
                $p=$this->M_permissions->get(null,$this->user_role_id,$m[0]->id_methods);
                if($p){
                    $this->acc_permission=true;
                    $pr=$this->M_privileges->get(null,$p[0]->id_permissions);
                    if($pr){
                        foreach($pr as $row)
                            array_push($this->acc_privilages,$row->actions);
                    }
                }else
                    $this->acc_permission=false;
            }
        }

        if(!$this->acc_permission)
            show_error("Maaf, role akun Anda tidak memiliki permission dan privileges yang sesuai untuk mengakses halaman ini.",403,"Access Restricted!");
    }

    function _render_breadcrumb(){
        $result=null;$no=0;$links=null;
        $url=$this->uri->segment_array();
        $result.='<li><a href="'.base_url().'"><i class="fa-home"></i>Beranda</a></li>';
        foreach($url as $row){
            $rx=str_replace('_',' ',$row);
            if($row=='sys')
                $rx='System Configuration';
            if($no<(count($url)-1)){
                $links.=$row.'/';
                $result.='<li><a href="'.base_url($links).'">'.ucwords($rx).'</a></li>';
                $no++;
            }else
                $result.='<li>'.ucwords($rx).'</li>';

        }
        $this->sys_breadcrumb=$result;
    }

    function _get_functions($file) {
        $functionFinder = '/function[\s\n]+(\S+)[\s\n]*\(/';
        $functionArray = array();
        $fileContents = file_get_contents($file);
        preg_match_all( $functionFinder , $fileContents , $functionArray );
        if( count( $functionArray )>1 ){
            $functionArray = $functionArray[1];
        }
        return $functionArray;
    }
}