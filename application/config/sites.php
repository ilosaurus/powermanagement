<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['name']             = 'Control and Power Management';
$config['author']           = 'Dimas Wicaksono';
$config['author_contact']   = 'diazinmotion@gmail.com';
$config['author_repo']      = 'https://github.com/diazinmotion';
$config['version']	        = '1.0.0';
$config['last_modified']    = '09 NOV 2016';