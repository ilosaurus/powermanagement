<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends HC_Controller {

	function __construct(){
		$this->acc_permission_bypass=1;
		$this->acc_login_bypass=1;
		parent::__construct();
	}

	public function index(){
		if(!$this->user_logged_in){
			$this->load->view('welcome_message');
		}else{
			if(uri_string()=='welcome')
				$this->load->view('welcome_message');
			else
				echo modules::run('dashboard/dashboard/index');
		}
	}
}
