<?php $this->load->view('includes/header') ?>
<body class="page-body">
<div class="page-container">
    <?php $this->load->view('includes/sidebar') ?>
    <div class="main-content">
        <div class="row">
            <div class="col-md-6 col-sm-8 clearfix"></div>
            <div class="col-md-6 col-sm-4 clearfix hidden-xs">
                <ul class="list-inline links-list pull-right">
                    <li class="profile-info">
                        <a href="#">
                            <img src="<?php echo base_url('assets/backend') ?>/images/useravatar.png" alt="" class="img-circle" width="40" />
                            <span style="margin-left: 10px;"><b><?php echo $this->user_full_name ?></b> &middot; <?php echo $this->user_role_name ?></span>
                        </a>
                    </li>
                    <li class="sep"></li>
                    <li><a href="<?php echo base_url('logout') ?>">Log Out <i class="entypo-logout right"></i></a></li>
                </ul>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bc-3"><?php echo $this->sys_breadcrumb; ?></ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6" style="line-height:60px; height:60px;">
                <?php if(isset($title) and $title!='Dashboard'){ ?>
                    <h2><?php echo $title ?></h2>
                <?php } ?>
            </div>
            <div class="col-lg-6 text-right" style="line-height:60px; height:60px;">
                <?php if(isset($action) and $action!=null){ ?>
                    <?php echo $action; ?>
                <?php } ?>
            </div>
        </div>
        <br/>
        <?php if(isset($content)) $this->load->view($content); else echo "Page Not Found." ?>
        <footer class="main" style="margin-top: 180px;">
            &copy; 2016 <strong><?php echo $this->config->item('name') ?></strong> - <a href="mailto:<?php echo $this->config->item('author_contact') ?>" target="_blank"><?php echo $this->config->item('author') ?></a>
        </footer>
    </div>
</div>
<?php $this->load->view('includes/footer') ?>