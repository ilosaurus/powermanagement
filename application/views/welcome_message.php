<!DOCTYPE html>
<html lang="en-us" class="no-js">
	<head>
		<meta charset="utf-8">
		<title>It Works! - Codeigniter 3.0</title>
		<meta name="description" content="The description should optimally be between 150-160 characters.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="ThemeHelite">
        <link rel="shortcut icon" href="<?php echo base_url('assets/frontend') ?>/img/favicon.png">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend') ?>/css/style.css" />
	</head>
	<body class="bubble">
        <canvas id="canvasbg"></canvas>
        <canvas id="canvas"></canvas>

        <div class="content">
            <div class="content-box">
                <div class="big-content">
                    <div class="list-square">
                        <span class="square"></span>
                        <span class="square"></span>
                        <span class="square"></span>
                    </div>
					
					<div class="list-line">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                    <i class="fa fa-search color" aria-hidden="true"></i>
                    <div class="clear"></div>
                </div>
                <h1>It Works!</h1>
                <p>Hello Devs, this is your first CI page.<br>
                   If you can see this page then you're ready to code.</p>
            </div>
        </div>
        <footer class="light">
            <ul>
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><a href="https://codeigniter.com">Webpage</a></li>
                <li><a href="<?php echo base_url('login') ?>">Login</a></li>
            </ul>
        </footer>

        <script src="<?php echo base_url('assets/frontend') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/frontend') ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/frontend') ?>/js/bubble.js"></script>
    </body>
</html>