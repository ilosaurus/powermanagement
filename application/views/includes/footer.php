<script src="<?php echo base_url('assets/backend') ?>/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/bootstrap.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/toastr.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/datatables/datatables.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/select2/select2.min.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/neon-custom.js"></script>
<script src="<?php echo base_url('assets/backend') ?>/js/neon-demo.js"></script>

<?php if(isset($alert)){ ?>
<script type="text/javascript">
    $(document).ready(function($) {
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        <?php if($alert['state']=='success'){ ?>
            toastr.success("<?php echo $alert['msg'] ?>","<?php echo $alert['title'] ?>", opts);
        <?php }if($alert['state']=='error'){ ?>
            toastr.error("<?php echo $alert['msg'] ?>", "<?php echo $alert['title'] ?>", opts);
    <?php } ?>
    });
</script>
<?php } ?>
</body>
</html>