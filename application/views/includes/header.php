<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php if(isset($title)) echo $title.' - ' ?><?php echo $this->config->item('name') ?></title>

	<link rel="icon" href="<?php echo base_url('assets/backend') ?>/images/favicon.png">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/css/custom.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/rickshaw/rickshaw.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/datatables/datatables.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/select2/select2.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/backend') ?>/js/selectboxit/jquery.selectBoxIt.css">
</head>