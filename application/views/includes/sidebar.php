<div class="sidebar-menu">
    <div class="sidebar-menu-inner">
        <header class="logo-env">
            <div class="logo">
                <a href="index.html">
                    <img src="<?php echo base_url('assets/backend') ?>/images/logo@2x.png" width="120" alt="" />
                </a>
            </div>
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>
        </header>

        <ul id="main-menu" class="main-menu">
            <li class="active">
                <a href="<?php echo base_url() ?>">
                    <i class="entypo-home"></i>
                    <span class="title">Beranda</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('makanan') ?>">
                    <i class="entypo-book"></i>
                    <span class="title">Makanan</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('minuman')?>">
                    <i class="entypo-book"></i>
                    <span class="title">Minuman</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('ala_carte')?>">
                    <i class="entypo-book"></i>
                    <span class="title">Ala Carte</span>
                </a>
            </li>
            <li class="has-sub">
                <a href="#">
                    <i class="entypo-cog"></i>
                    <span class="title">System</span>
                </a>
                <ul>
                    <li class="has-sub">
                        <a href="#">
                            <span class="title">Access</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('sys/module') ?>">
                                    <span class="title">Modules</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('sys/methods') ?>">
                                    <span class="title">Methods</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a href="#">
                            <span class="title">Permission &amp; Privileges</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('sys/permissions') ?>">
                                    <span class="title">Permissions</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('sys/privilages') ?>">
                                    <span class="title">Privileges</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('sys/division') ?>">
                            <span class="title">Division</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('sys/roles') ?>">
                            <span class="title">Roles</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('sys/users') ?>">
                            <span class="title">Users</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>